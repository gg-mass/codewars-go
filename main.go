package main

import (
	"errors"
	"fmt"
	"math"
	"strconv"
	"strings"
)

func NthFib(n int) float64 {
	phi := (1 + math.Sqrt(5)) / 2
	return math.Round(math.Pow(phi, float64(n)) / math.Sqrt(5))
}

func BinarySearch(array []int, target int, lowIndex int, highIndex int) int {
	if highIndex < lowIndex || len(array) == 0 {
		return -1
	}
	mid := (highIndex + lowIndex) / 2
	if array[mid] > target {
		return BinarySearch(array, target, lowIndex, mid-1)
	} else if array[mid] < target {
		return BinarySearch(array, target, mid+1, highIndex)
	} else {
		return mid
	}
}

func highAndLow(in string) string {
	var highest, lowest int

	for idx, char := range strings.Fields(in) {
		num, _ := strconv.Atoi(char)
		if idx == 0 {
			highest = num
			lowest = num
		}

		if num > highest {
			highest = num
		}

		if num < lowest {
			lowest = num
		}
	}
	return fmt.Sprintf("%d %d", highest, lowest)
}

func factorial(n int) (int, error) {
	if n < 0 {
		return 0, errors.New("undefined for negative numbers")
	}

	result := 1
	for i := n; i > 0; i-- {
		result *= i
	}
	return result, nil
}

func DNAStrand(dna string) string {
	translations := map[string]string{
		"A": "T",
		"T": "A",
		"C": "G",
		"G": "C",
	}
	result := ""

	for _, symbol := range dna {
		result += translations[string(symbol)]
	}

	return result
}

func StockList(listArt []string, listCat []string) string {
	counts := map[string]int{}
	for _, cat := range listCat {
		counts[cat] = 0
	}
	for _, book := range listArt {
		count, _ := strconv.Atoi(string(strings.Split(book, " ")[1]))
		counts[string(book[0])] += count
	}
	ret := ""
	for key, value := range counts {
		ret = ret + "(" + key + " : " + strconv.Itoa(value) + ") "
		if key != listCat[len(listCat)-1] {
			ret += "- "
		}
	}
	return ret
}

func MaxMultiple(d, b int) int {
	return (b / d) * d
}

func SpinWords(str string) string {
	ret := ""
	for _, word := range strings.Fields(str) {
		if len(word) < 5 {
			ret += word + " "
		} else {
			reversed := ""
			for _, char := range word {
				reversed = string(char) + reversed
			}
			ret += reversed + " "
		}
	}
	return ret[:len(ret)-1]
}

func EndsWith(str, ending string) bool {
	return str[len(str)-len(ending):] == ending
}

func DigitalRoot(n int) int {
	//https://www.wikiwand.com/en/Digital_root
	return 1 + (n-1)%9
}

func FindUniq(arr []float32) float32 {
	seen := map[float32]int{}

	for _, x := range arr {
		seen[x] += 1
	}

	for y := range seen {
		if seen[y] == 1 {
			return y
		}
	}
	return float32(0)
}

func GetRunesMap(word string) map[rune]int {
	characters := make(map[rune]int)
	for _, char := range word {
		_, exists := characters[char]
		if exists {
			characters[char] += 1
		} else {
			characters[char] = 1
		}
	}
	return characters
}

func Anagrams(word string, words []string) []string {
	chars := GetRunesMap(word)
	var ret []string
	for _, str := range words {
		if fmt.Sprint(chars) == fmt.Sprint(GetRunesMap(str)) {
			ret = append(ret, str)
		}
	}
	if len(ret) == 0 {
		return nil
	}
	return ret
}

func MaxBall(v0 int) int {
	height := 0.0
	lastHeight := -1.0
	for t := 0; true; t++ {
		height = float64(v0*t) - 4.905*float64(t*t)
		fmt.Println(t, height)
		if height < lastHeight {
			return t
		}
		lastHeight = height
	}
	return 0
}

func Numericals(s string) string {
	ret := ""
	seen := map[rune]int{}
	for _, char := range s {
		_, exists := seen[char]
		if exists {
			seen[char] += 1
		} else {
			seen[char] = 1
		}
		ret += fmt.Sprint(seen[char])
	}
	return ret
}

func MaxMap(dic map[string]int) string {
	var maxValue int
	var maxKey string
	for key, value := range dic {
		//fmt.Println(key, maxKey, value >= maxValue)
		if value >= maxValue { //returns the last item in case of equal
			maxValue = value
			maxKey = key
		}
	}
	return maxKey
}

func High(s string) string {
	scores := map[string]int{}
	for _, word := range strings.Fields(s) {
		scores[word] = 0
		for _, r := range word {
			scores[word] += (int(r) - 96) //ascii to alphabet rank
		}
	}
	//fmt.Println(scores)
	return MaxMap(scores)
}

func MajorityElement(nums []int) []int {
	seen := map[int]int{}
	ret := []int{}
	for _, num := range nums {
		_, exists := seen[num]
		if exists {
			seen[num] += 1
		} else {
			seen[num] = 1
		}
	}
	for key := range seen {
		if seen[key] > len(nums)/3 {
			ret = append(ret, key)
		}
	}
	return ret
}

func IsPowerOfTwo(n int) bool {
	return n != 0 && (n&(n-1)) == 0
}

func Reverse(x int32) int {
	var reversed string
	str := strconv.Itoa(int(x))
	if x < 0 {
		reversed += "-"
		str = str[1:]
	}
	for i := len(str) - 1; i >= 0; i-- {
		reversed += string(str[i])
	}
	ret, _ := strconv.Atoi(reversed)
	return ret
}

func Collatz(n int) int {
	if n%2 != 0 {
		n = n*3 + 1
	} else {
		n /= 2
	}
	if n == 1 {
		return 1
	}
	fmt.Println(n)
	return Collatz(n)
}

func main() {
	fmt.Println("Hello world")
}
